package ru.baulina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX");
        @Nullable final Integer index = TerminalUtil.nexInt() -1;
        System.out.println("ENTER PROJECT ID:");
        @Nullable final Long projectId = TerminalUtil.nexLong();
        @Nullable final SessionDTO session = getSession();
        endpointLocator.getTaskEndpoint().removeOneTaskByIndex(session, projectId, index);
        System.out.println("[OK]");
        System.out.println();
    }

}
