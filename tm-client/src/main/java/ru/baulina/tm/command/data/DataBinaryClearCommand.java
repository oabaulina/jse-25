package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.SessionDTO;

public final class DataBinaryClearCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-bin-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete bin data file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BINARY DELETE]");
        @Nullable final SessionDTO session = getSession();
        endpointLocator.getAdminDampEndpoint().dataBinaryClear(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
