package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.SessionDTO;

public final class DataJsonSaveCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save json to binary file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA JSON SAVE]");
        @Nullable final SessionDTO session = getSession();
        endpointLocator.getAdminDampEndpoint().dataJasonSave(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
