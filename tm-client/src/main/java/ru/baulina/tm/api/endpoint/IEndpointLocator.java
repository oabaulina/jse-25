package ru.baulina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.endpoint.*;

public interface IEndpointLocator {

    @NotNull
    AdminDumpEndpoint getAdminDampEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

}
