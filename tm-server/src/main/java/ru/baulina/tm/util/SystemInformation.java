package ru.baulina.tm.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class SystemInformation {

    public static int getAvailableProcessors() {
        return Runtime.getRuntime().availableProcessors();
    }

    public static long getFreeMemory() {
        return Runtime.getRuntime().freeMemory();
    }

    public static long getMaxMemory() {
        return Runtime.getRuntime().maxMemory();
    }

    public static long getTotalMemory() {
        return Runtime.getRuntime().totalMemory();
    }

    public static long getUsedMemoryFormat() {
        return getTotalMemory() - getFreeMemory();
    }

}