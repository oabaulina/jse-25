package ru.baulina.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.service.*;
import ru.baulina.tm.endpoint.*;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.service.*;

import javax.xml.ws.Endpoint;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IServiceLocator serviceLocator = this;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final SessionService sessionService = new SessionService(serviceLocator);

    @NotNull
    private final IUserService userService = new UserService();

    @NotNull
    private final ITaskService taskService = new TaskService();

    @NotNull
    private final IProjectService projectService = new ProjectService();

    @NotNull
    private final IDomainService domainService = new DomainService(userService, projectService, taskService);

    @NotNull
    private final AdminDumpEndpoint adminDampEndpoint = new AdminDumpEndpoint(serviceLocator);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(serviceLocator);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(serviceLocator);

    @SneakyThrows
    private void registryEndpoints(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServiceHost();
        final Integer port = propertyService.getServicePort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initUsers()  {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void initEndpoints() {
        registryEndpoints(adminUserEndpoint);
        registryEndpoints(adminDampEndpoint);
        registryEndpoints(projectEndpoint);
        registryEndpoints(sessionEndpoint);
        registryEndpoints(taskEndpoint);
        registryEndpoints(userEndpoint);
    }

    public void run(final String[] args) throws Exception {
        initEndpoints();
        initUsers();
    }

}
