package ru.baulina.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Random;

@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
public abstract class AbstractEntityDTO implements Serializable {

    private long id = Math.abs(new Random().nextLong());

}
