package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.endpoint.ISessionEndpoint;
import ru.baulina.tm.api.service.IServiceLocator;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.dto.UserDTO;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Objects;

@WebService
public class SessionEndpoint implements ISessionEndpoint {

    private IServiceLocator serviceLocator;

    public SessionEndpoint() {
        super();
    }

    public SessionEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        @Nullable Session session = serviceLocator.getSessionService().open(login, password);
        @Nullable SessionDTO sessionDTO = new SessionDTO();
        return sessionDTO.sessionDTOfrom(session);
    }

    @Override
    @WebMethod
    public void closeSession(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().close(session);
    }

    @Override
    @WebMethod
    public void closeAllSession(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().closeAll(session);
    }

    @Override
    @WebMethod
    public UserDTO getUser(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        @Nullable User user = serviceLocator.getUserService().findById(session.getUserId());
        @Nullable UserDTO userDTO = new UserDTO();
        return Objects.requireNonNull(userDTO.userDTOfrom(user));
    }

    @Override
    @WebMethod
    public List<Session> getListSession(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        return serviceLocator.getSessionService().getListSession(session);
    }

}
