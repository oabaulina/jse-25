package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.endpoint.IProjectEndpoint;
import ru.baulina.tm.api.service.IServiceLocator;
import ru.baulina.tm.dto.ProjectDTO;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.dto.TaskDTO;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
public class ProjectEndpoint implements IProjectEndpoint {

    private IServiceLocator serviceLocator;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public List<ProjectDTO> getProjectList() {
        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAll();
        @Nullable List<ProjectDTO> projectListDTO = new ArrayList<>();
        projectList.forEach((project) -> projectListDTO.add((new ProjectDTO()).projectDTOfrom(project)));
        return projectListDTO;
    }

    @Override
    @WebMethod
    public void loadProjects(
            @WebParam(name = "projects", partName = "projects") List<Project> projects
    ) {
        serviceLocator.getProjectService().load(projects);
    }

    @Override
    @WebMethod
    public ProjectDTO createProject(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        @Nullable final User user = serviceLocator.getUserService().findById(session.getUserId());
        @Nullable ProjectDTO projectDTO = new ProjectDTO();
        @Nullable final Project project =  serviceLocator.getProjectService().create(user, name);
        return projectDTO.projectDTOfrom(project);
    }

    @Override
    @WebMethod
    public ProjectDTO createProjectWithDescription(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        @Nullable final User user = serviceLocator.getUserService().findById(session.getUserId());
        @Nullable ProjectDTO projectDTO = new ProjectDTO();
        @Nullable final Project project = serviceLocator.getProjectService().create(user, name, description);
        return projectDTO.projectDTOfrom(project);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "project", partName = "project") Project project
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(project);
    }

    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjects(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        @Nullable final Long userId = session.getUserId();
        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAll(userId);
        @Nullable List<ProjectDTO> tprojectListDTO = new ArrayList<>();
        projectList.forEach((project) -> tprojectListDTO.add((new ProjectDTO()).projectDTOfrom(project)));
        return tprojectListDTO;
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @Override
    @WebMethod
    public ProjectDTO findOneProjectById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "id", partName = "id") Long id
    ) {
        serviceLocator.getSessionService().validate(session);
        @Nullable ProjectDTO projectDTO = new ProjectDTO();
        @Nullable final Project project = serviceLocator.getProjectService()
                .findOneById(session.getUserId(), id);
        return projectDTO.projectDTOfrom(project);
    }

    @Override
    @WebMethod
    public ProjectDTO findOneProjectByIndex(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        @Nullable ProjectDTO projectDTO = new ProjectDTO();
        @Nullable final Project project = serviceLocator.getProjectService()
                .findOneByIndex(session.getUserId(), index);
        return projectDTO.projectDTOfrom(project);
    }

    @Override
    @WebMethod
    public ProjectDTO findOneProjectByName(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        @Nullable ProjectDTO projectDTO = new ProjectDTO();
        @Nullable final Project project = serviceLocator.getProjectService()
                .findOneByName(session.getUserId(), name);
        return projectDTO.projectDTOfrom(project);
    }

    @Override
    @WebMethod
    public void removeOneProjectById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "id", partName = "id") Long id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeOneById(session.getId(), id);
    }

    @Override
    @WebMethod
    public void removeOneProjectByIndex(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeOneProjectByName(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "id", partName = "id") Long id,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().updateProjectById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "index", partName = "index") Integer index,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().updateProjectByIndex(session.getUserId(), index, name, description);
    }

}
