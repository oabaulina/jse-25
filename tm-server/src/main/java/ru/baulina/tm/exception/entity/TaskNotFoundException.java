package ru.baulina.tm.exception.entity;

public class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}