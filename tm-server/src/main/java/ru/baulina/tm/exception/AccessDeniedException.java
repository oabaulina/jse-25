package ru.baulina.tm.exception;

public class AccessDeniedException extends RuntimeException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
