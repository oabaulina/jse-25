package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.IRepository;
import ru.baulina.tm.api.repository.IUserRepository;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.AccessDeniedException;
import ru.baulina.tm.exception.empty.*;
import ru.baulina.tm.repository.UserRepository;
import ru.baulina.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull private final PropertyService propertyService = new PropertyService();

    @NotNull private final EntityManagerFactoryService entityManagerFactoryService
            = new EntityManagerFactoryService(propertyService);

    @Override
    protected IRepository<User> getRepository() {
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        return new UserRepository(em);
    }

    @Nullable
    @Override
    public List<User> findListUsers() {
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserRepository(em);
            @NotNull List<User> listUsert = repository.findAll();
            em.getTransaction().commit();
            return listUsert;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Override
    public @Nullable User findUser(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserRepository(em);
            @Nullable User user = repository.findUser(login, password);
            em.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            System.out.println(e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Override
    public User findById(@Nullable final Long id) {
        if (id == null || id < 0) throw new EmptyIdException();
         @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserRepository(em);
            @Nullable User user = repository.findById(id);
            em.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserRepository(em);
            @Nullable final User user = repository.findByLogin(login);
            em.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            System.out.println(e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Override
    public void removeById(@Nullable final Long id) {
        if (id == null || id < 0) throw new EmptyIdException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserRepository(em);
            repository.removeById(id);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserRepository(em);
            repository.removeByLogin(login);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeUser(@Nullable final User user) {
        if (user == null) return;
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserRepository(em);
            repository.removeUser(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserRepository(em);
            @NotNull final User user = new User();
            user.setRole(Role.USER);
            user.setLogin(login);
            user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password)));
            em.persist(user);
            em.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Nullable
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserRepository(em);
            @NotNull final User user = new User();
            user.setRole(Role.USER);
            user.setLogin(login);
            user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password)));
            user.setEmail(email);
            em.persist(user);
            em.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Nullable
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserRepository(em);
            @Nullable final User user = new User();
            user.setRole(role);
            user.setLogin(login);
            user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password)));
            em.persist(user);
            em.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Override
    public void lockUserLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserRepository(em);
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) return;
            user.setLocked(true);
            em.merge(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void unlockUserLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserRepository repository = new UserRepository(em);
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) return;
            user.setLocked(false);
            em.merge(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void changePassword(
            @Nullable final String passwordOld,
            @Nullable final String passwordNew,
            @Nullable final Long userId
    ) {
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            final User user = findById(userId);
            if (user == null) throw new AccessDeniedException();
            final String hashOld = HashUtil.salt(passwordOld);
            if (hashOld == null) throw new AccessDeniedException();
            if (!hashOld.equals(user.getPasswordHash())) throw new AccessDeniedException();
            final String hashNew = HashUtil.salt(passwordNew);
            user.setPasswordHash(hashNew);
            em.merge(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void changeUser(
            @Nullable final String email,
            @Nullable final String festName,
            @Nullable final String LastName,
            @Nullable final Long userId
    ) {
        if (userId == null) throw new EmptyUserIdException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            final User user = findById(userId);
            user.setEmail(email);
            user.setFirstName(festName);
            user.setLastName(LastName);
            em.merge(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

}
