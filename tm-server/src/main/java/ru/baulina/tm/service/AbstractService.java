package ru.baulina.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.IRepository;
import ru.baulina.tm.api.service.IService;
import ru.baulina.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import java.util.List;

@RequiredArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull private final PropertyService propertyService = new PropertyService();

    @NotNull private final EntityManagerFactoryService entityManagerFactoryService
            = new EntityManagerFactoryService(propertyService);

    protected abstract IRepository<E> getRepository();

    @Nullable
    @Override
    public List<E> findAll() {
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IRepository<E> repository = getRepository();
            @NotNull List<E> listEntites = repository.findAll();
            em.getTransaction().commit();
            return listEntites;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Override
    public void load(@Nullable List<E> entities) {
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IRepository<E> repository = getRepository();
            repository.clear();
            repository.merge(entities);
            em.getTransaction().commit();
       } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public Long count() {
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IRepository<E> repository = getRepository();
            @NotNull Long count = repository.count();
            em.getTransaction().commit();
            return count;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

}
