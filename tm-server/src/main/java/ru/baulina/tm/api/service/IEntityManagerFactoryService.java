package ru.baulina.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IEntityManagerFactoryService {

    EntityManagerFactory getEntityManagerFactory();

    EntityManager getEntityManager();

    EntityManagerFactory factory(@NotNull IPropertyService propertyService);

}
