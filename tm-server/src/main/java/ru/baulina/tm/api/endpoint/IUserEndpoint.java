package ru.baulina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.dto.UserDTO;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @WebMethod
    UserDTO createUser(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "login", partName = "login") String login,
            @WebParam(name = "password", partName = "password") String password
    );

    @WebMethod
    UserDTO createUserWithEmail(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "login", partName = "login") String login,
            @WebParam(name = "password", partName = "password") String password,
            @WebParam(name = "email", partName = "email") String email
    );

    @WebMethod
    UserDTO createUserWithRole(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "login", partName = "login") String login,
            @WebParam(name = "password", partName = "password") String password,
            @WebParam(name = "role", partName = "role") Role role
    );

    @WebMethod
    void changeUserPassword(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "passwordOld", partName = "passwordOld") String passwordOld,
            @WebParam(name = "passwordNew", partName = "passwordNew") String passwordNew,
            @WebParam(name = "userId", partName = "userId") Long userId
    );

    @WebMethod
    void profileOfUserChange(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "email", partName = "email") String email,
            @WebParam(name = "firstName", partName = "firstName") String firstName,
            @WebParam(name = "LastName", partName = "LastName") String LastName,
            @WebParam(name = "userId", partName = "userId") Long userId
    );

}
